-- This file is loaded from "LFGjoin.toc"

local f = CreateFrame("Frame")
f:SetScript("OnEvent",function(self,event)
  self:UnregisterEvent("LFG_LIST_SEARCH_RESULTS_RECEIVED")
  LFGListFrame.SearchPanel.SearchBox:SetText(self.search)
  C_LFGList.Search(6,self.search)
end)

SLASH_LFGJOIN1 = "/lfgjoin"
SlashCmdList["LFGJOIN"] = function(msg)
  f.search = msg
  if not GroupFinderFrame:IsVisible() then
    PVEFrame_ShowFrame("GroupFinderFrame")
  end
  GroupFinderFrameGroupButton4:Click()
  LFGListCategorySelection_SelectCategory(LFGListFrame.CategorySelection,6,0)
  LFGListCategorySelectionFindGroupButton_OnClick(LFGListFrame.CategorySelection.FindGroupButton)
  f:RegisterEvent("LFG_LIST_SEARCH_RESULTS_RECEIVED")
end
