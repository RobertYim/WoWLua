#!/usr/bin/env sh

# FONT="/Users/niko/Library/Fonts/msyhbd.ttf"
FONT='/Users/niko/Downloads/PingFang6.ttf'
WOWFONTSFOLDER='/Applications/World of Warcraft/Fonts'

if [[ ! -e $FONT ]]; then
    echo "Font file is missing.\nAbort."
    exit 1
fi

echo "Using ${FONT}"

# if [[ -e $WOWFONTSFOLDER ]]; then
#     echo "Fonts exists, you should delete wow fonts first."
# else
#     mkdir $WOWFONTSFOLDER
#     echo "Copying fonts"
# fi

### for zhCN
# cp "${FONT}" "${WOWFONTSFOLDER}"/ZYKai_T.ttf
# cp "${FONT}" "${WOWFONTSFOLDER}"/ZYHei.ttf
# cp "${FONT}" "${WOWFONTSFOLDER}"/ZYKai_C.ttf

### for zhTW
# cp "${FONT}" "${WOWFONTSFOLDER}"/bHEI00M.ttf
# cp "${FONT}" "${WOWFONTSFOLDER}"/bHEI01B.ttf
# cp "${FONT}" "${WOWFONTSFOLDER}"/bKAI00M.ttf
# cp "${FONT}" "${WOWFONTSFOLDER}"/bLEI00D.ttf

### for enUS
cp "${FONT}" "${WOWFONTSFOLDER}"/FRIZQT__.ttf
cp "${FONT}" "${WOWFONTSFOLDER}"/MORPHEUS.ttf
cp "${FONT}" "${WOWFONTSFOLDER}"/ARIALN.ttf
cp "${FONT}" "${WOWFONTSFOLDER}"/friends.ttf
cp "${FONT}" "${WOWFONTSFOLDER}"/skurri.ttf


echo "\nAll done."
