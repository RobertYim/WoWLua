-- UI

-- menu bar 技能栏
-- 隐藏主技能栏左右两旁的狮鹫头
MainMenuBarLeftEndCap:Hide()
MainMenuBarRightEndCap:Hide()

-- 主技能栏 UI 缩放
MainMenuBar:SetScale(0.75)
-- MultiBarBottomRight:SetScale(0.85)
-- MultiBarBottomLeft:SetScale(0.85)
-- 右边技能栏 UI 缩放
MultiBarLeft:SetScale(0.8)
MultiBarRight:SetScale(0.8)
-- 姿态栏 UI 缩放
-- Shapeshift:SetScale(0.85)

--[能量位置修改]
-- 圣骑
-- PaladinPowerBar:ClearAllPoints()
-- PaladinPowerBar:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- PaladinPowerBar:SetScale(1.0)
-- PaladinPowerBar:SetAlpha(1.0)

-- 萨满
-- TotemFrame:ClearAllPoints()
-- TotemFrame:SetPoint("RIGHT", CastingBarFrame, "LEFT", -10, 0)
-- TotemFrame:SetScale(1.0)
-- TotemFrame:SetAlpha(1.0)

-- 牧师
-- PriestBarFrame:ClearAllPoints()
-- PriestBarFrame:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- PriestBarFrame:SetScale(1.0)
-- PriestBarFrame:SetAlpha(1.0)

-- DK
-- RuneFrame:ClearAllPoints()
-- RuneFrame:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- RuneFrame:SetScale(1.0)
-- RuneFrame:SetAlpha(1.0)

-- EclipseBarFrame:ClearAllPoints()
-- EclipseBarFrame:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- EclipseBarFrame:SetScale(1.0)
-- EclipseBarFrame:SetAlpha(1.0)

-- 术士
-- WarlockPowerFrame:ClearAllPoints()
-- WarlockPowerFrame:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- WarlockPowerFrame:SetScale(1.0)
-- WarlockPowerFrame:SetAlpha(1.0)

-- 武僧
-- MonkHarmonyBar:ClearAllPoints()
-- MonkHarmonyBar:SetPoint("TOP", CastingBarFrame, "BOTTOM", 0, 0)
-- MonkHarmonyBar:SetScale(1.0)
-- MonkHarmonyBar:SetAlpha(1.0)

--[调整玩家到团队框架最后一位]
LoadAddOn("Blizzard_CompactRaidFrames") CRFSort_Group=function(t1, t2) if UnitIsUnit(t1,"player") then return false elseif UnitIsUnit(t2,"player") then return true else return t1 < t2 end end CompactRaidFrameContainer.flowSortFunc=CRFSort_Group

-- system default loss control frame
-- 系统 丢失控制 提示框位置
LossOfControlFrame:ClearAllPoints() LossOfControlFrame:SetPoint("CENTER",UIParent,"CENTER",0,-200)
-- 系统 丢失控制 提示框缩放
LossOfControlFrame:SetScale(0.8)
-- select(5,LossOfControlFrame:GetRegions()):ClearAllPoints() select(5,LossOfControlFrame:GetRegions()):SetPoint("TOP",select(4,LossOfControlFrame:GetRegions()),"BOTTOM")


-- Show movement speed in Person panel
-- 个人面板显示移动速度数值
-- http://nga.178.com/read.php?tid=9727518&_ff=7
do
local tempstatFrame
hooksecurefunc("PaperDollFrame_SetMovementSpeed",function(statFrame, unit)
   if(tempstatFrame and tempstatFrame~=statFrame)then
      tempstatFrame:SetScript("OnUpdate",nil);
   end
   statFrame:SetScript("OnUpdate", MovementSpeed_OnUpdate);
   tempstatFrame = statFrame;
   statFrame:Show();
end)
PAPERDOLL_STATINFO["MOVESPEED"].updateFunc =  function(statFrame, unit) PaperDollFrame_SetMovementSpeed(statFrame, unit); end
table.insert(PAPERDOLL_STATCATEGORIES[1].stats,{ stat = "MOVESPEED" })
end


-- Hide talking head
-- hooksecurefunc("TalkingHeadFrame_PlayCurrent", function()
	-- TalkingHeadFrame:Hide()
-- end)

